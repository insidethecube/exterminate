﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace Exterminate_V0._1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        //Basic Gamestate enum, for gamestate switching
        enum EGameState
        {
            Splash,
            Game,
            GameOver,
        }

        // This Puts the game in splash screen at start
        EGameState gameState = EGameState.Splash;
        
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

		// Bullet texture2d and bullet list
		Texture2D bulletTexture;
        
        // Bullet and enemies list
        // TO DO: When new bullet types are added( enemy bullets) new lists needed
		List<Bullet> bullets = new List<Bullet>();
        List<Enemy> enemies = new List<Enemy>();

		Texture2D player1Texture;
		Player player1;
		// string dalekDirTex = "dalek left";
        // TO DO : create  a switch that changes the dalek texture based on the last button pressed (left or right)
        // This will probably be in the player class update function im guessing

        Texture2D GameOverTexture;
        Texture2D SplashScreenTexture;
        Texture2D BackgroundTexture;
        Texture2D PlatformTexture;
        Texture2D BannerUpTexture;
        Texture2D BannerDownTexture;

        public static Vector2 PlatformPos = new Vector2(200, 200);

        // These are here to solve a structure problem in the player class (avoiding using preferredbackbuffer.width)
        public static int ScreenWidth = 800;
        public static int ScreenHeight = 600;


        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // This sets the screen to 800x600 no fullscreen

            //The variables that go into setting the screen size are defined in the same place as Texture2D, above
            this.graphics.PreferredBackBufferWidth = ScreenWidth;
            this.graphics.PreferredBackBufferHeight = ScreenHeight;
            this.graphics.IsFullScreen = false;
            graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

			GameOverTexture = Content.Load<Texture2D>("Exterminate! Gameover");
			bulletTexture = Content.Load<Texture2D>("laserGreen");
			player1Texture = Content.Load<Texture2D> ("dalek left");
			SplashScreenTexture = Content.Load<Texture2D>("Exterminate! Splash");
            BackgroundTexture = Content.Load<Texture2D>("BlankBG");
            PlatformTexture = Content.Load<Texture2D>("paddle");
            BannerUpTexture = Content.Load<Texture2D>("BannerT");
            BannerDownTexture = Content.Load<Texture2D>("BannerB");

			player1 = new Player(player1Texture, new Vector2(300,400), this, this.Content); 
			
            player1.leftKey = Keys.A; 
			player1.rightKey = Keys.D; 
			 
        }

        

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            
            
            // This is the main update function for the game.
            // the ONLY thing in here should be the gamestate switch and any code that runs in 
            // all of the following three: splash screen, gameover screen and the regular game
            // So there probably should NOT BE ANY OTHER CODE HERE

            switch (gameState)
            {
                case EGameState.Splash:
                    UpdateSplash(gameTime); break;
                case EGameState.Game:
                    UpdateGame(gameTime); break;
                case EGameState.GameOver:
                    UpdateGameOver(gameTime); break;
                default:
                    UpdateSplash(gameTime); break;
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // This is the main draw function for the game.
            // the ONLY thing in here should be the gamestate switch and any code that runs in 
            // all of the following three: splash screen, gameover screen and the regular game
            // So there probably should NOT BE ANY OTHER CODE HERE

            switch (gameState)
            {
                case EGameState.Splash:
                    DrawSplash(gameTime); break;
                case EGameState.Game:
                    DrawGame(gameTime); break;
                case EGameState.GameOver:
                    DrawGameOver(gameTime); break;
                default:
                    DrawSplash(gameTime); break;
            }


            base.Draw(gameTime);
        }
        void UpdateSplash(GameTime gameTime)
        {
            // This is the update function for the splash screen.
            // Add any update here that is supposed to run in the splash screen
            // There is unlikely to be anything here but a statement to get it to switch to the game when a button is pressed
			if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                gameState = EGameState.Game;
            }

        }

        void DrawSplash(GameTime gameTime)
        {
            // This is the draw function for the splash screen.
            // Add anything in here that is supposed to be drawn in the splash screen
            // There is unlikely to be anything here but the code to draw the splash screen background
            spriteBatch.Begin();
            spriteBatch.Draw(SplashScreenTexture, new Rectangle(0, 0, 800, 600), Color.White);
            spriteBatch.End();
        }

        void UpdateGame(GameTime gameTime)
        {
            // This is the update function for the game
            // The vast majority of the code for the game will be in here


            // This is just for debugging purposes, it allows you to switch to the gameover state
			if (Keyboard.GetState().IsKeyDown(Keys.OemTilde))
            {
                gameState = EGameState.GameOver;
            }

			if (Keyboard.GetState().IsKeyDown(Keys.LeftControl))
			{
				gameState = EGameState.Splash;
			}
            // This calls the update function for Player. I dont think its necessary to have it in a loop, as there is only 1 player
            player1.Update(gameTime);

            // This calls the update for each of the enemies.
            // Eventually it will be used to remove enemies who move outside the viewscreen
            // Removing enemies who are shot or hit the player is handled in CD
            for (int i = enemies.Count - 1; i >= 0; i--)
            {
                enemies[i].Update(gameTime);
            
            // This is if statement that will remove enemies who are outside a certain range
            // It should be used, once we figure out where the enemies will spawn. Forcing the game to update things we arent looking at will create lag
            // TO DO: Rather than 550, that should be preferred back buffer width - Banner(Bottom).Height
            // Probably have to make an int variable to do the calculation as i doubt it will work in the actual if statement
            //    if (enemies[i].position.Y > 550)
            //    {
            //        enemies.RemoveAt(i);
            //    }
            
            }


            // This calls the update function for each Playerbullet in turn
            for (int i = bullets.Count - 1; i >= 0; i--)
            {
                bullets[i].Update(gameTime);
                // This removes bullets that go outside the screen.
                // TO DO: Modify this to adjust based on the size of the banner/gui
                if (bullets[i].bulletpos.X < 0 || bullets[i].bulletpos.X > graphics.PreferredBackBufferWidth ||
                    bullets[i].bulletpos.Y < 0 ||
                    bullets[i].bulletpos.Y > graphics.PreferredBackBufferHeight)
                { bullets.RemoveAt(i); }
            }

        }

        void DrawGame(GameTime gameTime)
        {
            spriteBatch.Begin();
            // This background texture needs to be placed first, otherwise it will draw on top of everything else and ruin your day
            spriteBatch.Draw(BackgroundTexture, new Rectangle(0, 0, 800, 600), Color.White);
            spriteBatch.Draw(BannerUpTexture, new Vector2(0, 0), Color.White);
            spriteBatch.Draw(BannerDownTexture, new Vector2(0, 550), Color.White);

            player1.Draw(spriteBatch); 

            // Loop to draw all enemies in the list
			for (int i = 0; i < enemies.Count; i++) 
			{ 
				enemies[i].Draw(gameTime, spriteBatch); 
			}

            // Loop to draw all bullets in the list
			for (int i = 0; i < bullets.Count; i++) 
			{ 
				bullets[i].Draw(gameTime, spriteBatch); 
			}
            // This is just another placeholder
            spriteBatch.Draw(PlatformTexture, PlatformPos, Color.White);

            spriteBatch.End();
        }

        void UpdateGameOver(GameTime gameTime)
        {
			if (Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
				gameState = EGameState.Splash;
            }
        }

        void DrawGameOver(GameTime gameTime)
        {

            spriteBatch.Begin();
            spriteBatch.Draw(GameOverTexture, new Rectangle(0,0, 800, 600), Color.White);
            spriteBatch.End();
        }


		public void Shoot(Vector2 pos, Vector2 dir, float speed) 
		{ 
			Bullet b = new Bullet(bulletTexture, pos, dir, speed); 
			bullets.Add(b); 
		} 

    }
}
