﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;
namespace Exterminate_V0._1
{
	public class Player
	{

		public Texture2D playertex; 
		public Vector2 playerpos; 

		public Keys leftKey = Keys.A;
		public Keys rightKey = Keys.D;
		public Keys shootKey = Keys.Space; 
		public Game1 game;




		public Player(Texture2D texture, Vector2 pos, Game1 game) 
		{ 
			this.playertex = texture; 
			this.playerpos = pos; 
			this.game = game;
		} 
		public void Update(GameTime gameTime) 
		{ 

			if (Keyboard.GetState().IsKeyDown(leftKey)) playerpos.X -= 5.0f; 
			if (Keyboard.GetState().IsKeyDown(rightKey)) playerpos.X += 5.0f; 
			if (Keyboard.GetState().IsKeyDown(shootKey)) 
			{ 
				game.Shoot(playerpos, new Vector2(-1, 0), 10); 
			} 
		} 
		public void Draw(SpriteBatch spriteBatch) 
		{ 
			Vector2 offset = new Vector2(playertex.Width / 2, 0); 
			spriteBatch.Draw(playertex, playerpos - offset, Color.White); 
		}

		public Player ()
		{
		}
	}
}

