﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace Exterminate_V0._1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        //Basic Gamestate enum, for gamestate switching
        enum EGameState
        {
            Splash,
            Game,
            GameOver,
        }

        EGameState gameState = EGameState.Splash;
        
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D GameOverTexture;
        Texture2D SplashScreenTexture;
        Texture2D BackgroundTexture;
        Texture2D PlayerTexture;
        Texture2D PlatformTexture;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            this.graphics.PreferredBackBufferWidth = 800;
            this.graphics.PreferredBackBufferHeight = 600;
            this.graphics.IsFullScreen = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            GameOverTexture = Content.Load<Texture2D>("GameOverScreen1");
            SplashScreenTexture = Content.Load<Texture2D>("SplashScreen1");
            BackgroundTexture = Content.Load<Texture2D>("BGT");
            PlayerTexture = Content.Load<Texture2D>("mario");
            PlatformTexture = Content.Load<Texture2D>("paddle");

            // TODO: use this.Content to load your game content here
        }

        

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            switch (gameState)
            {
                case EGameState.Splash:
                    UpdateSplash(gameTime); break;
                case EGameState.Game:
                    UpdateGame(gameTime); break;
                case EGameState.GameOver:
                    UpdateGameOver(gameTime); break;
                default:
                    UpdateSplash(gameTime); break;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // There shouldnt be anything else here other than this switch statement.
            // Code for updating or drawing goes in its specific update/draw function, at the end of the code.

            switch (gameState)
            {
                case EGameState.Splash:
                    DrawSplash(gameTime); break;
                case EGameState.Game:
                    DrawGame(gameTime); break;
                case EGameState.GameOver:
                    DrawGameOver(gameTime); break;
                default:
                    DrawSplash(gameTime); break;
            }

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
        void UpdateSplash(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                gameState = EGameState.Game;
            }
        }

        void DrawSplash(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(SplashScreenTexture, new Rectangle(0, 0, 800, 600), Color.White);
            spriteBatch.End();
        }

        void UpdateGame(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                gameState = EGameState.GameOver;
            }
        }

        void DrawGame(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(BackgroundTexture, new Rectangle(0, 0, 800, 600), Color.White);
            spriteBatch.End();
        }

        void UpdateGameOver(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }
        }

        void DrawGameOver(GameTime gameTime)
        {

            spriteBatch.Begin();
            spriteBatch.Draw(GameOverTexture, new Rectangle(0,0, 800, 600), Color.White);
            spriteBatch.End();
        }




    }
}
