﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace Exterminate_V0._1
{
	public class Bullet
	{
		public Texture2D bullettex; 
		public Vector2 bulletpos; 
		public Vector2 bulletdir; 
		public float speed; 

		public Bullet (Texture2D texture, Vector2 pos, Vector2 dir, float speed)
		{
			this.bullettex = texture; 
			this.bulletpos = pos; 
			this.bulletdir = dir; 
			this.speed = speed;

		}
		public void Update(GameTime gameTime) 
		{ 
			bulletpos += bulletdir * speed; 
		} 

		public void Draw(GameTime gameTime, SpriteBatch spriteBatch) 
		{ 
			Vector2 offset = new Vector2(bullettex.Width/2, bullettex.Height/2); 
			spriteBatch.Draw(bullettex, bulletpos - offset, Color.White); 
		} 
	}
}

