﻿
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace Exterminate_V0._1
{
	public class Enemy
	{
		public Texture2D enemytex; 
		public Vector2 enemypos; 

		public Enemy(Texture2D texture, Vector2 position) 
		{ 
			this.enemytex = texture; 
			this.enemypos = position; 
		} 

		public void Update(GameTime gameTime) 
		{ 
			enemypos.Y += 5.0f; 
		} 

		public void Draw(GameTime gameTime, SpriteBatch spriteBatch) 
		{ 
			spriteBatch.Draw(enemytex, enemypos, Color.White); 
		}  
	}
}

