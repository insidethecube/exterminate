﻿
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
namespace Exterminate_V0._1
{
	public class Player
	{

		public Texture2D playertex; 
		public Vector2 playerpos;

        // These textures are declared here because it is the simplest way to allow me to get
        // the texture.width or texture.height to function. Using it makes the CD modular, 
        //so it wont have to be changed when the real GUI is implemented
        Texture2D PlatformTexture;
        Texture2D BannerUpTexture;
        Texture2D BannerDownTexture;

		public Keys leftKey = Keys.A;
		public Keys rightKey = Keys.D;
		public Keys shootKey = Keys.Space; 
		public Game1 game;

        // This might look a bit strange, but afaik, its one of the simplest ways to get texture.width to work properly in this class.
		public Player(Texture2D texture, Vector2 pos, Game1 game, ContentManager content) 
		{ 
			this.playertex = texture; 
			this.playerpos = pos; 
			this.game = game;
            this.PlatformTexture = content.Load<Texture2D>("paddle");
            this.BannerUpTexture = content.Load<Texture2D>("BannerT");
            this.BannerDownTexture = content.Load<Texture2D>("BannerB");
		} 
		public void Update(GameTime gameTime) 
		{ 

			// This is where we change the player left/right move speed
            // TO DO: add a switch to change the player sprite to left or right facing depending on the last button pressed
            if (Keyboard.GetState().IsKeyDown(leftKey)) playerpos.X -= 5.0f; 
			if (Keyboard.GetState().IsKeyDown(rightKey)) playerpos.X += 5.0f; 
            //Adding temporary keystrokes to test vertical movement and debugging
            if (Keyboard.GetState().IsKeyDown(Keys.W)) playerpos.Y -= 5.0f;
            if (Keyboard.GetState().IsKeyDown(Keys.S)) playerpos.Y += 5.0f; 
			
            // Basic shoot function
            if (Keyboard.GetState().IsKeyDown(shootKey)) 
			{ 
				game.Shoot(playerpos, new Vector2(-1, 0), 10); 
			}

            // I would put these variables outside of the update function if i could, so they dont clog up this update
            // It doesnt seem to allow it though
            int BannerUpWidth = BannerUpTexture.Width;
            int BannerUpHeight = BannerUpTexture.Height;
            int BannerDownWidth = BannerDownTexture.Width;
            int BannerDownHeight = BannerDownTexture.Height;
            int PlatformWidth = PlatformTexture.Width;
            int PlatformHeight = PlatformTexture.Height;
            int BannerDownAdjustedY = Game1.ScreenHeight - BannerDownHeight;

            // Player Collision detection vs walls/floor - totally modular, so it wont require changes when the GUI changes
            //These are the actual rectangles used, below are the if statements that stop the player from moving beyond them.

            Rectangle PlayerRectangle = new Rectangle((int)playerpos.X, (int)playerpos.Y, playertex.Width, playertex.Height);
            Rectangle BannerUpRectangle = new Rectangle(0, 0, BannerUpWidth, BannerUpHeight);
            Rectangle BannerDownRectangle = new Rectangle(0, BannerDownAdjustedY, BannerDownWidth, BannerDownHeight);
            // This is the CD for player to Platform
            // Depending on how the platforms are coded, it will change how we do this. We may just create a rectangle statement like
            // the one below for each one, or we could loop through a list

            Rectangle PlatformRectangle = new Rectangle(Convert.ToInt32(Game1.PlatformPos.X),
                Convert.ToInt32(Game1.PlatformPos.Y), PlatformWidth, PlatformHeight);

            //This stops the player from moving too far up and down under the GUI or offscreen
            if (Rectangle.Intersect(PlayerRectangle, BannerUpRectangle) != Rectangle.Empty)
            {
                playerpos.Y = BannerUpHeight;
            }
            if (Rectangle.Intersect(PlayerRectangle, BannerDownRectangle) != Rectangle.Empty)
            {
                playerpos.Y = Game1.ScreenHeight - BannerDownHeight - playertex.Height;  //Banner height minus texture height
            }
            // This is needed because the Draw function of the player texture uses as offset 
            int offset = playertex.Width / 2;
            // This stops the player from moving outside the screen to the left and right
            if (playerpos.X <= 0 + offset)
            {
                playerpos.X = 0 + offset;
            }
            if (playerpos.X >= Game1.ScreenWidth - playertex.Width + offset)
            {
                playerpos.X = Game1.ScreenWidth - playertex.Width + offset;
            }

            // Im going to use this later for Collision detection between platforms and player its not working atm
            // Its more complex than regular collision because i have to give a different order to playerpos when it hits each side of the platform
            //if (PlayerRectangle.Right > PlatformRectangle.Left && PlayerRectangle.Right < PlatformRectangle.Right)
            //{
            //    playerpos.X = Game1.PlatformPos.X - offset - playertex.Width;
            //}
            // This is for the Plaform to Player CD


		} 
		public void Draw(SpriteBatch spriteBatch) 
		{ 
			Vector2 offset = new Vector2(playertex.Width / 2, 0); 
			spriteBatch.Draw(playertex, playerpos - offset, Color.White); 
		}
	}
}

